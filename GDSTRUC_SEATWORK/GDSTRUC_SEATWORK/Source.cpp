#include <iostream>
#include <string>

using namespace std;

//Item #1
void addDigits(int input, int sum) {
	if (input <= 0) {
		cout << "Total sum of digits: " << sum << endl;
		return;
	}

	sum += input % 10;
	addDigits(input/10, sum);
}

//Item #2
void printFibonacci(int numOfTerms, int no1, int no2, int sum) {
	if (numOfTerms <= 0) {
		return;
	}

	cout << sum << " ";
	sum = no1 + no2;
	no1 = no2;
	no2 = sum;
	printFibonacci(numOfTerms - 1, no1, no2, sum);
}

//Item #3

void checkIfPrime(int input, int num) {
	bool ifPrime = true;
	if (input % num == 0) {
		ifPrime = false;
		cout << "This is not a prime number" << endl;
		return;
	}
	else {
		ifPrime = true;
		if (num <= input / 2) {
			checkIfPrime(input, num + 1);
		}
		else {
			cout << "This is a prime number" << endl;
			return;
		}
	}
}

int main() {
	// #1
	int input;
	int sum = 0;

	cout << "Input Number:" << endl;
	cin >> input;

	addDigits(input, sum);

	cout << endl;

	// #2
	int term1 = 0;
	int term2 = 1;
	int sumOfTerms = 1;
	cout << "Input number of terms:" << endl;
	cin >> input;

	cout << "Fibonacci Sequence:" << endl;
	printFibonacci(input, term1, term2, sumOfTerms);

	cout << endl;

	cout << endl;
	
	// #3
	int divisibleNum = 2;
	cout << "Input Number:" << endl;
	cin >> input;
	
	checkIfPrime(input, divisibleNum);

	system("pause");
}