#include <iostream>
#include <string>
#include <time.h>

#include "Stack.h"
#include "Queue.h"

using namespace std;

void main()
{
	cout << "Enter size of array: ";
	int size;
	cin >> size;
	Queue<int> queueArray(size);
	Stack<int> stackArray(size);

	int choice;
	
	cout << endl << endl;
	
	while (true) {
		cout << "What do you want to do?" << endl;
		cout << "1 - Push elements" << endl;
		cout << "2 - Pop elements" << endl;
		cout << "3 - Print everything and empty set" << endl;
		cin >> choice;
		cout << endl << endl;

		if (choice > 3 || choice < 1) {
			system("cls");
		}

		switch (choice) {
		case 1:
			int input;
			cout << "Enter Number: " ;
			cin >> input;
			
			queueArray.push(input);
			stackArray.push(input);
			
			cout << endl;
			cout << "Top of element sets:" << endl;
			cout << "Queue: " << queueArray.top() << endl;
			cout << "Stack: " << stackArray.top() << endl;

			system("pause");
			system("cls");
			break;
		case 2:
			cout << "You have popped the front elements" << endl << endl;
			queueArray.pop();
			stackArray.pop();

			cout << "Top of element sets:" << endl;
			cout << "Queue: " << queueArray.top() << endl;
			cout << "Stack: " << stackArray.top() << endl;
			system("pause");
			system("cls");
			break;
		case 3:
			cout << "Queue Elements:" << endl;
			for (int x = queueArray.getSize() - 1; x >= 0; x--) {
				cout << queueArray[x] << endl;
				queueArray.pop();
			}

			cout << "Stack Elements:" << endl;
			for (int x = stackArray.getSize() - 1; x >= 0; x--) {
				cout << stackArray[x] << endl;
				stackArray.pop();
			}
			system("pause");
			system("cls");
			break;
		}
	}
}