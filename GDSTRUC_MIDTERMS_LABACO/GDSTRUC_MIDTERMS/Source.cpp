#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size); //commented

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	cout << "\nGenerated array: " << endl;
		int choice;

		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";

		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";

		cout << endl << endl;
		cout << "What do you want to do?" << endl;
		cout << "1 - Remove element at index" << endl;
		cout << "2 - Search for Element" << endl;
		cout << "3 - Expand and generate random values" << endl;
		cin >> choice;
		cout << endl << endl;

		switch (choice) {
		case 1:
			int input;
			cout << "Enter index to remove:" << endl;
			cin >> input;
			unordered.remove(input);
			ordered.remove(input);
			cout << "Element removed: " << input << endl;
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";

			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";
			cout << endl;
			system("pause");
			system("cls");

			break;
		case 2:
			cout << "\n\nEnter number to search: ";
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;
			system("pause");
			system("cls");
			break;
		//case 3:
			//break;
		}
}